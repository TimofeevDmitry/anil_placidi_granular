clear;
clc;

pp0 = 'D:\Documents\PhD\PROJECTS\GRANULAR\ANIL_PLACIDI_GRANULAR_Git\results';
force_disp_multiple(pp0, 'H', 200);

% pp1 = 'D:\Documents\PhD\PROJECTS\GRANULAR\ANIL_PLACIDI_GRANULAR_Git\results\no_fixed_point';
% force_disp_multiple(pp1, 'H', 100);

L = 0.01;
S = 0.1;

Bt = 3.5e-8;
Bw = 5e-8;

du = -2e-9;
N = 799;

u_bar = 0:du:N*du;
G11 = (u_bar ./ (S)) .* (-1 + u_bar ./ (2*S));
G12 = zeros(size(u_bar));
G21 = zeros(size(u_bar));
G22 = zeros(size(u_bar));

N_theta = 3600;
dtheta = (2*pi/N_theta);
theta = (0:dtheta:2*pi - dtheta)';

norm = [ cos(theta), sin(theta)];

u_eta = zeros(N, N_theta);
D_eta = zeros(N, N_theta);
for i = 1:length(u_bar)
    
        
    n1 = norm(:, 1) .* norm(:, 1);
    n2 = norm(:, 1) .* norm(:, 2);
    n3 = norm(:, 2) .* norm(:, 1);
    n4 = norm(:, 2) .* norm(:, 2);
    
    u_eta(i, :) = 2*L*(G11(i) .* n1 + G12(i) .* n2 + G21(i) .* n3 + G22(i) .*n4);
    D_eta(i, :) = 1 - exp(-0.5 * u_eta(i, :)/Bt);
    
    if i > 1
        cond = D_eta(i,:) < D_eta(i - 1, :);
        D_eta(i, cond) = D_eta(i - 1, cond);
    end
end

u_tau = zeros(N, N_theta);
D_tau = zeros(N, N_theta);
for i = 1:length(u_bar)
    
    u1 = G11(i) * norm(:, 1) + G12(i) * norm(:, 2); 
    u2 = G21(i) * norm(:, 1) + G22(i) * norm(:, 2);
    
    u_tau(i, :) = sqrt(4 * L^2 * (u1 .^ 2 + u2 .^ 2) - (squeeze(u_eta(i, :))') .^ 2);
    D_tau(i, :) = 1 - exp(-abs(u_tau(i, :))/Bw);
    
    if i > 1
        cond = D_tau(i, :) < D_tau(i - 1, :);
        D_tau(i, cond) = D_tau(i - 1, cond);
    end
end

kn = 14e+13;
kw = 3.0e+13;

RF = zeros(size(u_bar));
for i = 1:length(u_bar)
    C_val = 0;
    for dir = 1:1:N_theta
        n1 = 1;
        n1 = n1 * norm(dir, 1) * norm(dir, 1);
        n1 = n1 * norm(dir, 1) * norm(dir, 1);
        
        n2 = norm(dir, 1) * norm(dir, 1);
        n3 = norm(dir, 1) * norm(dir, 1);
        n4 = norm(dir, 1) * norm(dir, 1);
        n5 = norm(dir, 1) * norm(dir, 1);
        
        C1 = kn * (1 - D_eta(i, dir)) * n1;
        C2 = kw * (1 - D_tau(i, dir))* ((n2 + n3 + n4 + n5) - 4 * n1);
        
        C_val = C_val + C1 + C2;
    end
    C_val = L^2 * C_val * dtheta;
    
    RF(i) = (C_val * G11(i));
end

DD = max(max(D_eta));
DT = max(max(D_tau));

plot(-u_bar, RF, '--b', 'LineWidth', 2);
grid on;
