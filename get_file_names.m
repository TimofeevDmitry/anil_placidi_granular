function [file_names] = get_file_names()
    input_folder = [pwd, filesep, 'input'];
    if exist(input_folder, 'dir')
        file_names = dir([input_folder, filesep, '*.json']);
    else
        file_names = [];
    end
end