function update_console(msg)
    persistent nbytes;
    if isempty(nbytes)
        nbytes = 0;
    end
    if nbytes > 0
        fprintf(repmat('\b', 1, nbytes));
    end
    nbytes = fprintf(msg);
end