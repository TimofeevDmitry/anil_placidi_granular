clear;
clc;

x = -1:0.01:1;

a1 = 10;
a2 = 30;

Bw0 = 1;

alpharatio = ((1 - a1) / a2) * Bw0;

dn1 = x >= 0;
dn2 = (x >= alpharatio) & (x < 0);
dn3 = (x  < alpharatio) & (x < 0);

Bw_val = dn1 * Bw0;
Bw_val = Bw_val + dn2 .* (Bw0 - a2 * x);
Bw_val = Bw_val + dn3 .* (a1 * Bw0);

set(groot,'defaultAxesTickLabelInterpreter','latex'); 

plot(x, Bw_val, 'LineWidth', 2);
set(gca,'YTickLabel',[]);
set(gca, 'FontSize', 18);
% set(gca,'XTickLabel',[]);

xticks([alpharatio 0])
xticklabels({'$\frac{1-\alpha_1}{\alpha_2}\cdot B_{\tau0}$','0'})

grid on;
hold on;

% determine position of the axes
axp = get(gca,'Position');

% determine startpoint and endpoint for the arrows 
xs=axp(1) + axp(3)/2;
xe=axp(1) + axp(3) + 0.05;
ys=axp(2) + 0.1;
ye=axp(2) + axp(4);

annotation('arrow', [axp(1) xe], [ys ys]);
annotation('arrow', [xs xs],[ys ye]);

axis([-1 1 0 11])

% xlabel('$u^{el}_{\eta}$', 'interpreter', 'latex', 'FontSize',18);
% ylabel('$B_{\tau}$', 'interpreter', 'latex', 'FontSize',18, 'rotation',0, 'HorizontalAlignment','Right');