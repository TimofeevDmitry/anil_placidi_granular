function force_disp_multiple(folder, type, n_disp_cols,  NP, par_name, order)
    type_c = upper(char(type));
    if ~strcmp(type_c, 'H') && ~strcmp(type_c, 'V') && ~strcmp(type_c, 'ABS')
        return;
    end
    
    legend_info = [];
    
    content = dir(folder);
    
    cc = hsv(length(content) + 1);

    for i = 1:length(content)
        if ~content(i).isdir || strcmp(content(i).name, '.') ...
                || strcmp(content(i).name, '..')
            continue;
        end
        
        fd_folder = [folder filesep content(i).name filesep 'data'];
        
        try
            force_disp = importdata([fd_folder, filesep, 'force_disp.txt']);
        catch ex
            disp(getReport(ex));
            continue;
        end
        
        % size of arrays
        N = size(force_disp);
        N = N(1);
        
        if nargin > 3
            N = min([N, NP]);
        end

        disp_data = zeros(N, 1);
        for j = 1:n_disp_cols
            disp_data = disp_data + force_disp(1:N, j) .^ 2;
        end
        disp_data = sqrt(disp_data);
        
        try 
            f_hor = -force_disp(1:N, n_disp_cols + 1);
        catch
            disp([content(i).name ': no column for horizontal component of force']);
            f_hor = zeros(N);
        end
        
        try 
            f_ver = -force_disp(1:N, n_disp_cols + 2);
        catch
            disp([content(i).name ': no column for vertical component of force']);
            f_ver = zeros(N);
        end
        
        f_abs = sqrt(f_hor.^2 + f_ver.^2);
        
        switch type_c
            case 'H'
                f_val = f_hor;
            case 'V'
                f_val = f_ver;
            case 'ABS'
                f_val = f_abs;
        end
        
                l_type = ["-", "-."];

        plot(disp_data, f_val, 'LineWidth', 2);
        hold on;
        grid on;

        if nargin > 4 && ~strcmp(par_name, '-')
            try
                text = fileread([folder filesep content(i).name ...
                    filesep 'parameters.json']);
                params = jsondecode(text);

                par_val = params.(par_name);
                
                line = string(par_name) + " = " + string(par_val);
                
                legend_info = [legend_info line];
            catch ex
                displ_data(getReport(ex));
                displ_data('\n');
            end
        end
    end
    
    if nargin > 4 && ~strcmp(par_name, '-')
        legend(legend_info, 'Interpreter', 'Latex', 'FontSize', 14);
    end
    
    if nargin > 5 && ~ischar(order)
        chH = get(gca,'Children');
        set(gca, 'Children', chH(order));
    end
    
    xlabel('Imposed displacement $\bar{u}$', 'Interpreter', 'Latex');
    ylabel('Reaction Force', 'Interpreter', 'Latex');
    
    set(gca, 'FontSize', 16)
end

