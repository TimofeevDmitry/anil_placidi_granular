clear;
clc;

% folder, where a model is located
model_folder = ...
    'E:\PhD\WWW\09-May-2022_07.33.03_OMEN_PC\models';

data_folder = ...
    'E:\PhD\WWW\09-May-2022_07.33.03_OMEN_PC\data';

iters = 0:1:480;
for i = iters

    model = mphload([model_folder filesep sprintf('model_%d.mph', i)]);

    model.result.table.create('tbl1', 'Table');

    model.result.numerical.create('int1', 'IntSurface');
    model.result.numerical('int1').set('intvolume', true);
    model.result.numerical('int1').remove('unit', [0 1]);
    model.result.numerical('int1').remove('descr', [0 1]);
    model.result.numerical('int1').remove('expr', [0 1]);
    model.result.numerical('int1').setIndex('expr', 'W(x, y)', 0);
    model.result.numerical('int1').selection.set([1]);
    model.result.numerical('int1').set('table', 'tbl1');
    model.result.numerical('int1').setResult;

    td = model.result.table('tbl1').getReal();

    f_bar = model.param.get('f_bar');
    u_bar = model.param.get('u_bar');

    w_line = sprintf('%d %d %d', str2double(u_bar), str2double(f_bar), td);

    w_file = fopen([data_folder filesep 'W_disp.txt'], 'at');
    fprintf(w_file, "%s\n", w_line);
    fclose(w_file);

    fprintf('%d\t->\tW = %d\n', i, td);
end
