function E_max_plot_1(folder, n_disp_cols, IDX, ll)

mesh_size = [0.001, 0.002, 0.003, 0.005, 0.01];
e_max = zeros(1, 5);

    for i = 1:5
        e_folder = [folder filesep sprintf('%d', i) filesep 'data'];

        try
            e_disp = importdata([e_folder, filesep, 'e_disp.txt']);
        catch ex
            disp(getReport(ex));
            continue;
        end

        e_max(i) = e_disp(IDX, n_disp_cols + 1);

    end

    if nargin == 3 || ll
        loglog(mesh_size, e_max, 'LineWidth', 2);
    else
        plot(mesh_size, e_max, 'LineWidth', 2);
    end
hold on;
grid on;

% xlabel('max element size', 'Interpreter', 'Latex');
% ylabel('$E_{max}$', 'Interpreter', 'Latex');

set(gca, 'FontSize', 18)

end