function W_total_plot(folder, n_disp_cols)
     content = dir(folder);

      cc = hsv(length(content) + 1);

    for i = 1:length(content)
        if ~content(i).isdir || strcmp(content(i).name, '.') ...
                || strcmp(content(i).name, '..')
            continue;
        end

        w_folder = [folder filesep content(i).name filesep 'data'];
        
        try
            w_disp = importdata([w_folder, filesep, 'w_disp.txt']);
        catch ex
            disp(getReport(ex));
            continue;
        end

        s = size(w_disp);

        disp_data = zeros(s(1), 1);
        for j = 1:n_disp_cols
            disp_data = disp_data + w_disp(:, j) .^ 2;
        end
        disp_data = sqrt(disp_data);

        w_val = w_disp(:, n_disp_cols + 1);


        l_type = ["-", "--"];

        plot(disp_data, w_val, 'LineWidth', 2, 'Color', cc(i + mod(i, 2) + 1, :), 'LineStyle', l_type(mod(i, 2) + 1));
        hold on;
        grid on;
    end

    xlabel('Imposed displacement $\bar{u}$', 'Interpreter', 'Latex');
    ylabel('Total Dissipation energy $(W)$', 'Interpreter', 'Latex');

    set(gca, 'FontSize', 16)
end

