clear;
clc;

%% simulation parameters
L = 0.1;        % intergranular distance
ntheta = 120;   % discretization over orientation

%% coordinates of the points requested
requested_points = [
    0, 0;
    0, L;
    L, 0;
    L, L;
    (1 - 0.12)*L/2, L/2;
    (1 + 0.12)*L/2, L/2;
    L/2, (1 - 0.12)*L/2;
    L/2, (1 + 0.12)*L/2;
    (1 - 0.12/sqrt(2))*L/2, (1 - 0.12/sqrt(2))*L/2;
    (1 + 0.12/sqrt(2))*L/2, (1 - 0.12/sqrt(2))*L/2;
    (1 - 0.12/sqrt(2))*L/2, (1 + 0.12/sqrt(2))*L/2;
    (1 + 0.12/sqrt(2))*L/2, (1 + 0.12/sqrt(2))*L/2;
    L/2, L;
    L, L/2;
    L/2, 0;
    0, L/2;
    L/2, (L + (1 + 0.12)*L/2)/2;
    L/2, (1 - 0.12)*L/4;
    ];

%% titles to the points requested (titles for output figures)
titles = [
    "corner_left_bottom";
    "corner_left_top";
    "corner_right_bottom";
    "corner_right_top";
    "hole_left";
    "hole_right";
    "hole_bottom";
    "hole_top";
    "hole_left_bottom";
    "hole_right_bottom";
    "hole_left_top";
    "hole_right_top";
    "side_top";
    "side_right";
    "side_bottom";
    "side_left";
    "crack_top";
    "crack_bottom";
    ];

%% define input and output paths
folder_in = ...
    'D:\Documents\PhD\PROJECTS\GRANULAR\ANIL_PLACIDI_GRANULAR_Git\results\09-Aug-2021_14.04.04_DESKTOP-8VDHUIC\';
folder_out = ...
    'D:\Documents\PhD\PROJECTS\GRANULAR\ANIL_PLACIDI_GRANULAR_Git\results\09-Aug-2021_14.04.04_DESKTOP-8VDHUIC\figures\polar_plots';

%% define indices of the mesh points, corresponding to the requested coordinates
load([folder_in filesep 'data' filesep 'mesh.mat']);

s = size(points);
n_mesh = s(1);

pnts_size = size(requested_points);
if pnts_size(1) ~= length(titles)
    error('dimensions of "points" and "titles" do not coincide');
end
NP = pnts_size(1);

indices = zeros(1, NP);
for pnt_idx = 1:NP
    req_point = requested_points(pnt_idx, :);
    
    dist = realmax;
    for j = 1:1:n_mesh
        mesh_point = points(j, :);
        couple = [req_point; mesh_point];
        d = pdist(couple, 'euclidean');
        if d < dist
            dist = d;
            j_min = j;
        end
    end
    
    indices(pnt_idx) = j_min;
end

pnts.indices = indices;
pnts.titles = titles;

%% making damage plots
damage_in = [folder_in filesep 'data' filesep 'damage'];

iters.step = 5;
iters.max = 322;

format.line_width = 3;
format.font_size = 35;

format.color = 'red';
damage_polar_plots('N', damage_in, folder_out, pnts, iters, format);

format.color = 'blue';
damage_polar_plots('T', damage_in, folder_out, pnts, iters, format);

%% making plastic plots
plastic_in = [folder_in filesep 'data' filesep 'plasticity'];

iters.step = 5;
iters.max = 322;

format.line_width = 3;
format.font_size = 35;

format.color = 'green';
plastic_polar_plots('T', plastic_in, folder_out, pnts, iters, format);

format.color = 'magenta';
plastic_polar_plots('C', plastic_in, folder_out, pnts, iters, format);

