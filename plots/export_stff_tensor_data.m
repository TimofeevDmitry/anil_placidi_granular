clear;
clc;

% folder, where a model is located
model_folder = ...
    'E:\PhD\WWW\07-May-2022_20.35.27_OMEN_PC\models';

data_folder = ...
    'E:\PhD\WWW\07-May-2022_20.35.27_OMEN_PC\data';

name = 'C1111';

pntx = 0.5;
pnty = 0.5;

par_name = [name, '_pn'];
expr_str = sprintf('%s(%d, %d)', name, pntx, pnty);

fclose('all');
f_name = [data_folder filesep name '_disp.txt'];
if exist(f_name, 'file') ~= 0
    delete(f_name);
end

iters = 0:1:3;
for i = iters

    model = mphload([model_folder filesep sprintf('model_%d.mph', i)]);

    model.param.set(par_name, expr_str);
    c_val = model.param.evaluate(par_name);

    f_bar = model.param.evaluate('f_bar');
    u_bar = model.param.evaluate('u_bar');

    c_line = sprintf('%d %d %d', u_bar, f_bar, c_val);

    
    c_file = fopen(f_name, 'at');
    fprintf(c_file, "%s\n", c_line);
    fclose(c_file);

    fprintf('%d\t->\t%s = %d\n', i, expr_str, c_val);
end