function force_disp(folder, type, NP)
    type_c = upper(char(type));
    if ~strcmp(type_c, 'H') && ~strcmp(type_c, 'V') && ~strcmp(type_c, 'ABS')
        return;
    end
    
    legend_info = [];
    
    try
        data = importdata([folder, filesep, 'data', filesep, 'force_disp.txt']);
    catch ex
        display(getReport(ex));
        return;
    end
    
    % size of arrays
    N = size(data);
    N = N(1);
    
    if nargin > 2
        N = min([N, NP]);
    end
    
    try
        f_hor = -data(1:N, 2);
    catch
        display('No column for horizontal component of force');
        f_hor = zeros(N, 1);
    end
    
    try
        f_ver = -data(1:N, 3);
    catch
        display('No column for horizontal component of force');
        f_ver = zeros(N, 1);
    end
    
    f_abs = sqrt(f_hor.^2 + f_ver.^2);
    
    switch type_c
        case 'H'
            f_val = f_hor;
        case 'V'
            f_val = f_ver;
        case 'ABS'
            f_val = f_abs;
    end
    
    plot(data(1:N, 1), f_val, 'LineWidth', 2);
    hold on;
    grid on;
    
    if nargin > 3
        try
            text = fileread([folder '\parameters.json']);
            params = jsondecode(text);
            
            par_val = params.(par_name);
            
            line = string(par_name) + " = " + string(par_val);
            
            legend_info = [legend_info line];
        catch ex
            disp(getReport(ex));
            disp('\n');
        end
    end
    
    
    if nargin > 3
        legend(legend_info, 'Interpreter', 'Latex', 'FontSize', 10);
    end
    
    xlabel('Imposed displacement $\bar{u}$', 'Interpreter', 'Latex');
    ylabel('Reaction Force', 'Interpreter', 'Latex');
end

