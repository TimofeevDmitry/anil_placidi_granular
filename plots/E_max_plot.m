function E_max_plot(folder, n_disp_cols, N)
    content = dir(folder);

    for i = 1:length(content)
        if ~content(i).isdir || strcmp(content(i).name, '.') ...
                || strcmp(content(i).name, '..')
            continue;
        end

        e_folder = [folder filesep content(i).name filesep 'data'];
        
        try
            e_disp = importdata([e_folder, filesep, 'e_disp.txt']);
        catch ex
            disp(getReport(ex));
            continue;
        end

        s = size(e_disp);

        disp_data = zeros(s(1), 1);
        for j = 1:n_disp_cols
            disp_data = disp_data + e_disp(:, j) .^ 2;
        end
        disp_data = sqrt(disp_data);

        e_val = e_disp(:, n_disp_cols + 1);

        plot(disp_data(1:N), e_val(1:N), 'LineWidth', 2);
        hold on;
        grid on;
    end

%     xlabel('Imposed displacement $\bar{u}$', 'Interpreter', 'Latex');
%     ylabel('Maximum Elastic Energy Density $(E_{max})$', 'Interpreter', 'Latex');

    set(gca, 'FontSize', 16)
end

