clear;
clc;

folder = ...
    'D:\Documents\PhD\PROJECTS\GRANULAR\ANIL_PLACIDI_GRANULAR_Git\results\5';

data_folder = [folder filesep 'data'];
models_folder = [folder filesep 'models'];

N = 25;


for i=0:1:N
    model = mphopen([models_folder filesep sprintf('model_%d.mph', i)]);
    
    E_eval = mpheval(model, 'energy');
    E_max = max(E_eval.d1);

    u_bar = model.param.get('u_bar');

    E_line = sprintf('%d %d', str2double(u_bar), E_max);

    e_file = fopen([data_folder filesep 'e_disp.txt'], 'at');
    fprintf(e_file, "%s\n", E_line);
    fclose(e_file);

    fprintf('%d\t->\tE_max = %d\n', i, E_max);
end
