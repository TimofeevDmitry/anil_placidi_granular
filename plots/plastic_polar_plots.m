function plastic_polar_plots(type, folder, out_path, pnts, iter_info, format)
    type_c = char(type);
    if ~strcmpi(type_c, 'T') && ~strcmpi(type_c, 'C') && ~strcmpi(type_c, 'FULL')
        return;
    end
        
   
    for i = 1:length(pnts.indices)
        fig = figure('units','centimeters','position',[0, 0, 20, 20]);
        
        step_iter = 1;
        if nargin > 4 && ~ischar(iter_info)
            max_iter  = iter_info.max;
            step_iter = iter_info.step;
            if max_iter < step_iter
                [step_iter, max_iter] = deal([max_iter, step_iter]);
            end
        else
            max_iter = length(dir([folder filesep '*.mat'])) - 1;
        end
        
        for j = 1:step_iter:max_iter
            file_name = [folder filesep 'plasticity' sprintf('_%d.mat', j)];
            load(file_name, 'Lt', 'Lc');
            
            switch upper(type_c)
                case 'T'
                    plast = Lt(pnts.indices(i), :)';
                    style = '-r';
                case 'C'
                    plast = Lc(pnts.indices(i), :)';
                    style = '-b';
            end
            
            plast = [plast; plast(1)];

            dtheta = 2*pi / (length(plast) - 1);
            theta = (0: dtheta: 2 * pi);
            
            if nargin > 5
                polarplot(theta, plast, style, 'Color', format.color, ...
                    'LineWidth', format.line_width);
            else
                polarplot(theta, plast, style);
            end
            hold on;
        end
        
        title = char(pnts.titles(i));
        
        if nargin > 5
            set(gca, 'FontSize', format.font_size);
        end
        
        saveas(fig, [out_path filesep 'plast_' type_c '_' title '.png']);
        close(fig);
    end
end

