function pnts = spiral_sphere(n_theta, n_phi)
% n_theta - number of points for one turn
% n_phi - number of turns

% total number of points
dtheta = ((2 * pi) / n_theta);
dphi = (pi / n_phi) / n_theta;

theta = 0:dtheta:((2 * n_phi * pi));
phi = 0:dphi:pi;

X = cos(theta) .* sin(phi);
Y = sin(theta) .* sin(phi);
Z = cos(phi);

pnts = [X; Y; Z]';
end

