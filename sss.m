clear;
clc;

% folder, where a model is located
model_folder = ...
    'E:\PhD\WWW\09-May-2022_07.33.03_OMEN_PC\models';

% folder for exported_figures
out_folder = ...
    'E:\PhD\WWW\09-May-2022_07.33.03_OMEN_PC\figures_rescaled';

iters = [96, 192, 288, 384, 480];
for i = iters
    update_console(sprintf("loading model_%d...", i));
    model = mphload([model_folder filesep sprintf('model_%d.mph', i)]);
    
    max_val = containers.Map;
    max_val('energy') = 4;
    max_val('W') = 1.4;
    
    for s = ["energy", "W"]
        label = char(s);
        
        plot_tag = ['plot_' label];
        image_tag = ['image_' label];
        
        update_console(sprintf([label '_%d...'], i));
        
        model.result(plot_tag).feature('surf1').set('rangecoloractive', true);
        model.result(plot_tag).feature('surf1').set('rangecolormax', max_val(label));
        model.result(plot_tag).feature('surf1').set('rangecolormin', 0);
        model.result(plot_tag).run;
        
        out_filename = [out_folder filesep label filesep sprintf([label '_%d.png'], i)];
        model.result.export(image_tag).set('options2d', false);
        model.result.export(image_tag).set('zoomextents', true);
        model.result.export(image_tag).set('pngfilename', out_filename);
        model.result.export(image_tag).run;
    end
end
update_console('');