clear;
clc;

import com.comsol.model.*;
import com.comsol.model.util.*;

%% path settings

% folder, where a model is located
model_folder = ...
    'D:\Documents\PhD\PROJECTS\GRANULAR\ANIL_PLACIDI_GRANULAR_Git\comsol';

% name of an input comsol file
model_name = 'test_cylinder_3d';

%% initial settings and model loading

% dimention of our model (2 or 3)
n_dim = 3;

% load the model to the server
model = mphload([model_folder filesep model_name]);

model.param.set('L', '0.1[m]');

model.param.set('u_bar', '0');
model.param.set('f_bar', '0');

%% create tensor tables

tensor_labels = ["C", "M", "D", "P", "Q", "R"];

% create headers for tables
th = containers.Map();
for lbl = tensor_labels
    if n_dim == 2
        args = ["x" "y"];
    else
        args = ["x" "y" "z"];
    end
    th(lbl) = args;
end

% fill sets of headers
th("R") = [th("R") "R"];
for i = 1:n_dim
    for j = 1:n_dim
        th("P") = [th("P") sprintf("P%d%d", i, j)];
        for h = 1:n_dim
            th("Q") = [th("Q") sprintf("Q%d%d%d", i, j, h)];
            for a = 1:n_dim
                th("C") = [th("C") sprintf("C%d%d%d%d", i, j, h, a)];
                for b = 1:n_dim
                    th("M") = ...
                        [th("M") sprintf("M%d%d%d%d%d", i, j, h, a, b)];
                    for c = 1:n_dim
                        th("D") = [th("D") ...
                            sprintf("D%d%d%d%d%d%d", i, j, h, a, b, c)];
                    end
                end
            end
        end
    end
end 

% function units
fu = containers.Map();
if n_dim == 2
    fu("C") = "J*m^-2";
    fu("M") = "J*m^-1";
    fu("D") = "J";
    fu("P") = "J*m^-2";
    fu("Q") = "J*m^-1";
    fu("R") = "J*m^-2";
elseif n_dim == 3
    % ?
    fu("C") = "J*m^-3";
    fu("M") = "J*m^-2";
    fu("D") = "J*m^-1";
    fu("P") = "J*m^-3";
    fu("Q") = "J*m^-2";
    fu("R") = "J*m^-3";
end

% create tables
for lbl = tensor_labels
    % table for tensor
    table_tag = "table_" + lbl;
    model.result.table.create(table_tag, "Table");
    model.result.table(table_tag).label(table_tag);
    model.result.table(table_tag).setColumnHeaders(th(lbl));
    
    func = th(lbl);

    % set of functions for tensor components
    func_tag = lbl;
    model.func.create(func_tag, 'Interpolation');
    model.func(func_tag).label(['tensor_' char(lbl)]);
    model.func(func_tag).set('source', 'resultTable');
    model.func(func_tag).set('resultTable', table_tag);
    model.func(func_tag).set('nargs', n_dim);
    model.func(func_tag).set('argunit', 'm');
    model.func(func_tag).set('fununit', fu(lbl));
    model.func(func_tag).set('funcs', cellstr([func((n_dim + 1):end)' ...
        num2str((1:(length(func) - n_dim))', '%03d')]));
end

%% strain
if n_dim == 2
    model.variable('var1').set('G11', 'u1x + 0.5*(u1x^2 + u2x^2)');
    model.variable('var1').set('G12', '0.5*(u1y + u2x + u1x*u1y + u2x*u2y)');
    model.variable('var1').set('G21', 'G12');
    model.variable('var1').set('G22', 'u2y + 0.5*(u1y^2 + u2y^2)');
    
elseif n_dim == 3
    model.variable('var1').set('G11', 'u1x + 0.5*(u1x^2 + u2x^2 + u3x^2)');
    model.variable('var1').set('G12', '0.5*(u1y + u2x + u1x*u1y + u2x*u2y + u3x*u3y)');
    model.variable('var1').set('G13', '0.5*(u1z + u3x + u1x*u1z + u2x*u2z  +u3x*u3z)');
    
    model.variable('var1').set('G21', 'G12');
    model.variable('var1').set('G22', 'u2y + 0.5*(u1y^2 + u2y^2 + u3y^2)');
    model.variable('var1').set('G23', '0.5*(u2z + u3y + u1y*u1z + u2y*u2z + u3y*u3z)');
    
    model.variable('var1').set('G31', '0.5*(u3x + u1z + u1z*u1x + u2z*u2x + u3z*u3x)');
    model.variable('var1').set('G32', 'G23');
    model.variable('var1').set('G33', 'u3z + 0.5*(u1z^2 + u2z^2 + u3z^2)');
end

%% strain gradient
for i = 1:n_dim
	for j = 1:n_dim
        lbl=sprintf('G%d%d', i, j);
        for k = 1:n_dim
            model.variable('var1').set([lbl num2str(k)], ...
                ['d(' lbl ',' char(args(k)) ')']);
        end
	end
end

%% elastic energy

% function arguments
fn_args = '';
for k = 1:n_dim
    if isempty(fn_args)
        fn_args = char(args(k));
    else
        fn_args = [fn_args ', ' char(args(k))];
    end
end
fn_args = ['(' fn_args ')'];

% first gradient
energy_str = '';
for i = 1:n_dim
	for j = 1:n_dim
		for a = 1:n_dim
			for b = 1:n_dim
                fn = sprintf('C%d%d%d%d', i, j, a, b);
                gc = sprintf('G%d%d*G%d%d', i, j, a, b);
                
                en = [fn fn_args '*' gc];
               
				if isempty(energy_str)
					energy_str = en;
				else
					energy_str = [energy_str '+' en];
				end
			end
		end
	end
end
energy_str = ['0.5*(' energy_str ')'];
model.variable('var1').set('energy1', energy_str);

% chirality
energy_str = '';
for i = 1:n_dim
    for j = 1:n_dim
        for a = 1:n_dim
            for b = 1:n_dim
                for c = 1:n_dim
                    fn = sprintf('M%d%d%d%d%d', i, j, a, b, c);
                    gc = sprintf('G%d%d*G%d%d%d', i, j, a, b, c);
                    
                    en = [fn fn_args '*' gc];
                    
                    if isempty(energy_str)
                        energy_str = en;
                    else
                        energy_str = [energy_str '+' en];
                    end
                end
            end
        end
    end
end
model.variable('var1').set('energy2', energy_str);

% second gradient
energy_str = '';
for i = 1:n_dim
    for j = 1:n_dim
        for h = 1:n_dim
            for a = 1:n_dim
                for b = 1:n_dim
                    for c = 1:n_dim
                        fn = sprintf('D%d%d%d%d%d%d', i, j, h, a, b, c);
                        gc = sprintf('G%d%d%d*G%d%d%d', i, j, h, a, b, c);
                        
                        en = [fn fn_args '*' gc];
                        
                        if isempty(energy_str)
                            energy_str = en;
                        else
                            energy_str = [energy_str '+' en];
                        end
                    end
                end
            end
        end
    end
end
energy_str = ['0.5*(' energy_str ')'];
model.variable('var1').set('energy3', energy_str);

% pre-stress
energy_str = '';
for i = 1:n_dim
    for j = 1:n_dim
        fn = sprintf('P%d%d', i, j);
        gc = sprintf('G%d%d', i, j);
        
        en = [fn fn_args '*' gc];
        
        if isempty(energy_str)
            energy_str = en;
        else
            energy_str = [energy_str '+' en];
        end
    end
end
model.variable('var1').set('energy4', energy_str);

% pre-hyperstress
energy_str = '';
for i = 1:n_dim
    for j = 1:n_dim
        for h = 1:n_dim
            fn = sprintf('Q%d%d%d', i, j, h);
            gc = sprintf('G%d%d%d', i, j, h);
            
            en = [fn fn_args '*' gc];
            
            if isempty(energy_str)
                energy_str = en;
            else
                energy_str = [energy_str '+' en];
            end
        end
    end
end
model.variable('var1').set('energy5', energy_str);

% additional scalar
model.variable('var1').set('energy6', ['R' fn_args ]);

% total elastic energy
energy_str = '';
energy_str = [energy_str 'energy1 + energy2 + energy3 +'];
energy_str = [energy_str 'energy4 + energy5 + energy6'];
model.variable('var1').set('energy', energy_str);

%% relative displacement, damage, plasticity and dissipation energy
rel_disp.label = "OBJ_REL_DISP";
rel_disp.head = [args "u_eta" "u_tau"];
rel_disp.funcs = {
    'u_eta' '1';
    'u_tau' '2'
    };
rel_disp.fun_unit = 'm';

damage.label = "DAMAGE";
damage.head = [args "DMG_NORM_MAX" "DMG_NORM_AVR" ...
    "DMG_TANG_MAX" "DMG_TANG_AVR"];
damage.funcs = { 
    'DMG_NORM_MAX' '1';
    'DMG_NORM_AVR' '2';
    'DMG_TANG_MAX' '3';
    'DMG_TANG_AVR' '4'
    };
damage.fun_unit = '1';

plastic.label = "PLASTICITY";
plastic.head =  [args "LMT_MAX" "LMT_AVR" "LMC_MAX" ...
    "LMC_AVR" "PLASTIC_AVR"]; 
plastic.funcs = {
    'LMT_MAX'       '1';
    'LMT_AVR'       '2';
    'LMC_MAX'       '3';
    'LMC_AVR'       '4';
    'PLASTIC_AVR'   '5';
    };
plastic.fun_unit = '1';

w.label = "W";
w.head = [args "W"];
w.funcs = {'W' '1'};
w.fun_unit = 'J*m^-3';

for descr = [rel_disp, damage, plastic, w]
    table_tag = "table_" + descr.label;
    model.result.table.create(table_tag, "Table");
    model.result.table(table_tag).label(table_tag);
    model.result.table(table_tag).setColumnHeaders(descr.head);
    
    dummy_data = zeros(1, n_dim + length(descr.funcs));
    model.result.table(table_tag).setTableData(dummy_data);
    
    func_tag = descr.label;
    model.func.create(func_tag, 'Interpolation');
    model.func(func_tag).label(func_tag);
    model.func(func_tag).set('source', 'resultTable');
    model.func(func_tag).set('resultTable', table_tag);
    model.func(func_tag).set('nargs', n_dim);
    model.func(func_tag).set('argunit', 'm');
    model.func(func_tag).set('fununit', descr.fun_unit);
    model.func(func_tag).set('funcs', descr.funcs);
end

%% lagrange multiplier
% table for boundary probes 
% (bounday probes should be configured manually to put their output into the table)
table_tag = "table_lm";
model.result.table.create(table_tag, "Table");
model.result.table(table_tag).label(table_tag);

%% plots and images

% for now, only 2d case is implemented
if n_dim == 2
    % energies
    for lbl = ["energy", "energy1", "energy2", "energy3", ...
            "energy4", "energy5", "energy6"]
        % plot
        plot_tag = "plot_" + lbl;
        model.result.create(plot_tag, 'PlotGroup2D');
        model.result(plot_tag).label(lbl);
        model.result(plot_tag).create('surf1', 'Surface');
        model.result(plot_tag).feature('surf1').set('expr', lbl);
        model.result(plot_tag).feature('surf1').set('colortable', 'Spectrum');
        model.result(plot_tag).feature('surf1').set('smooth', 'none');
        model.result(plot_tag).feature('surf1').set('resolution', 'norefine');
        model.result(plot_tag).set('showlegendsmaxmin', true);

        % exported image
        image_tag = "image_" + lbl;
        model.result.export.create(image_tag, 'Image2D');
        model.result.export(image_tag).label(image_tag);
        model.result.export(image_tag).set('antialias', false);
        model.result.export(image_tag).set('legend', true);
        model.result.export(image_tag).set('imagetype', 'png');
        model.result.export(image_tag).set('plotgroup', plot_tag);
    end
    
    % displacement, strain and strain gradient components
    for lbl = ["u1", "u2", "G11", "G12", "G22", ...
            "G111", "G112", "G121", "G122", "G221", "G222"]
        % plot
        plot_tag = "plot_" + lbl;
        model.result.create(plot_tag, 'PlotGroup2D');
        model.result(plot_tag).label(lbl);
        model.result(plot_tag).create('surf1', 'Surface');
        model.result(plot_tag).feature('surf1').set('expr', lbl);
        model.result(plot_tag).feature('surf1').set('colortable', 'Spectrum');
        model.result(plot_tag).feature('surf1').set('smooth', 'none');
        model.result(plot_tag).feature('surf1').set('resolution', 'norefine');
        
        % exported image
        image_tag = "image_" + lbl;
        model.result.export.create(image_tag, 'Image2D');
        model.result.export(image_tag).label(image_tag);
        model.result.export(image_tag).set('antialias', false);
        model.result.export(image_tag).set('legend', true);
        model.result.export(image_tag).set('imagetype', 'png');
        model.result.export(image_tag).set('plotgroup', plot_tag);
    end
    
    % objective relative displacement
    for lbl = ["u_eta", "u_tau"]
        % plot
        plot_tag = "plot_" + lbl;
        model.result.create(plot_tag, 'PlotGroup2D');
        model.result(plot_tag).label(lbl);
        model.result(plot_tag).create('surf1', 'Surface');
        model.result(plot_tag).feature('surf1').set('expr', lbl + "(x,y)");
        model.result(plot_tag).feature('surf1').set('colortable', 'Spectrum');
        model.result(plot_tag).feature('surf1').set('smooth', 'none');
        model.result(plot_tag).feature('surf1').set('resolution', 'norefine');
        
        % exported image
        image_tag = "image_" + lbl;
        model.result.export.create(image_tag, 'Image2D');
        model.result.export(image_tag).label(image_tag);
        model.result.export(image_tag).set('antialias', false);
        model.result.export(image_tag).set('legend', true);
        model.result.export(image_tag).set('imagetype', 'png');
        model.result.export(image_tag).set('plotgroup', plot_tag);
    end
    
    % damage
    for lbl = ["DMG_NORM_MAX", "DMG_NORM_AVR", "DMG_TANG_MAX", "DMG_TANG_AVR"]   
        % plot
        plot_tag = "plot_" + lbl;
        model.result.create(plot_tag, 'PlotGroup2D');
        model.result(plot_tag).label(lbl);
        model.result(plot_tag).create('surf1', 'Surface');
        model.result(plot_tag).feature('surf1').set('expr', lbl + "(x,y)");
        model.result(plot_tag).feature('surf1').set('colortable', 'Thermal');
        model.result(plot_tag).feature('surf1').set('colortablerev', 'on');
        model.result(plot_tag).feature('surf1').set('rangecoloractive', 'on');
        model.result(plot_tag).feature('surf1').set('rangecolormax', '1');
        model.result(plot_tag).feature('surf1').set('rangecolormin', '0');
        model.result(plot_tag).feature('surf1').set('smooth', 'none');
        model.result(plot_tag).feature('surf1').set('resolution', 'norefine');
        model.result(plot_tag).set('showlegendsmaxmin', true);
        
        % exported image
        image_tag = "image_" + lbl;
        model.result.export.create(image_tag, 'Image2D');
        model.result.export(image_tag).label(image_tag);
        model.result.export(image_tag).set('antialias', false);
        model.result.export(image_tag).set('legend', true);
        model.result.export(image_tag).set('imagetype', 'png');
        model.result.export(image_tag).set('plotgroup', plot_tag);
    end
    
    
    % plasticity
    for lbl = ["LMT_MAX", "LMT_AVR", "LMC_MAX", "LMC_AVR", "PLASTIC_AVR"]       
        % plot
        plot_tag = "plot_" + lbl;
        model.result.create(plot_tag, 'PlotGroup2D');
        model.result(plot_tag).label(lbl);
        model.result(plot_tag).create('surf1', 'Surface');
        model.result(plot_tag).feature('surf1').set('expr', lbl + "(x,y)");
        model.result(plot_tag).feature('surf1').set('colortable', 'Spectrum');
        model.result(plot_tag).feature('surf1').set('smooth', 'none');
        model.result(plot_tag).feature('surf1').set('resolution', 'norefine');
        model.result(plot_tag).set('showlegendsmaxmin', true);
        
        % eported image
        image_tag = "image_" + lbl;
        model.result.export.create(image_tag, 'Image2D');
        model.result.export(image_tag).label(image_tag);
        model.result.export(image_tag).set('antialias', false);
        model.result.export(image_tag).set('legend', true);
        model.result.export(image_tag).set('imagetype', 'png');
        model.result.export(image_tag).set('plotgroup', plot_tag);
    end
    
    % dissipation energy
    lbl = "W";
  
    % plot    
    plot_tag = "plot_" + lbl;
    model.result.create(plot_tag, 'PlotGroup2D');
    model.result(plot_tag).label(lbl);
    model.result(plot_tag).create('surf1', 'Surface');
    model.result(plot_tag).feature('surf1').set('expr', lbl + "(x,y)");
    model.result(plot_tag).feature('surf1').set('colortable', 'Spectrum');
    model.result(plot_tag).feature('surf1').set('smooth', 'none');
    model.result(plot_tag).feature('surf1').set('resolution', 'norefine');
    model.result(plot_tag).set('showlegendsmaxmin', true);
    
    % create image to export
    image_tag = "image_" + lbl;
    model.result.export.create(image_tag, 'Image2D');
    model.result.export(image_tag).label(image_tag);
    model.result.export(image_tag).set('antialias', false);
    model.result.export(image_tag).set('legend', true);
    model.result.export(image_tag).set('imagetype', 'png');
    model.result.export(image_tag).set('plotgroup', plot_tag);
    
elseif n_dim == 3
    % ?
end

%%

% filename to save the model
out_model_name = [model_folder filesep model_name '_new.mph'];

% save model to mph file
model.save(out_model_name);

% remove the model from the server
ModelUtil.clear();