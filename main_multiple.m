clear;
clc;

echo off;

% ------------------------------
% IMPORT COMSOL API
% ------------------------------
import com.comsol.model.*
import com.comsol.model.util.*

% ------------------------------
% GETTING INPUT FILE NAMES
% ------------------------------
file_names = get_file_names();

% ------------------------------
% HOW MANY SIMULATIONS WE SHOULD DO
% ------------------------------
N = length(file_names);
if N == 0
    error('There are no files with input parameters');
end

for i = 1:1:N
    clear update_console;
    
    fprintf('Simulation %d (of %d) now in progress:\n', i, N);
    
    file_name = [file_names(i).folder, filesep, file_names(i).name];
    
    simulation = GRANULAR(file_name);
    
    % create set of output folders
    folders = simulation.create_folders();
    
    % copy input file
    copyfile(file_name, [folders.result, filesep, 'parameters.json']);
    
    tic;
    try
        % run the calculations
        simulation.run(folders);
    catch ex
        % clear model on comsol server
        ModelUtil.clear();
        
        % save log
        write_except_log(folders.result, ex);
        
        % display error
        fprintf(2, "\n%s\n\n", ex.getReport());
        
        % error sound
        beep;
        
        % try next simulation
        continue;
    end
    elapsed_time = toc;
    
    % notify in case of success
    fprintf('Simulation has finished with no errors!\n');
    
    % notify about elapsed time in seconds and write it in the log file
    time_msg = sprintf('Elapsed time is %d seconds.\n\n', round(elapsed_time));
    write_log(folders.result, time_msg);
    fprintf(time_msg);
end