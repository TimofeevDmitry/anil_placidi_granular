clear;
clc;

plastic_folder = ...
    'E:\PhD\10 cycles\08-Feb-2021_00.29.20_Computer\data\plasticity';

snt = 2e+6;
snc = 2e+7;

W    = zeros(1, 1610);
u_pl = zeros(1, 1610);

for i = 1:1610
    file_name = [plastic_folder filesep sprintf('plasticity_%d.json', i)];
    plast_obj = jsondecode(fileread(file_name));
    
    s = size(plast_obj);
    
    W(i) = (2 * pi / s(1)) * ...
        (snt * (sum(plast_obj.Lt(:, 1))) + snc * (sum(plast_obj.Lc(:, 1))));
    
    u_pl(i) = mean(plast_obj.Lt(:, 1) - plast_obj.Lc(:, 1));
    
end



subplot(2, 1, 1);
plot(1:1610, u_pl, 'LineWidth', 2);
title('$\bar{u}^{pl}_{\eta}(t)$', 'Interpreter', 'latex', 'FontSize', 20);
xticks(0:161:1610);
xticklabels({'0', '1','2','3','4','5','6','7', '8', '9', '10'})
xlabel('cycles') 
grid on;

subplot(2, 1, 2);
plot(1:1610, W, 'LineWidth', 2);
title('$W_{pl}$', 'Interpreter', 'latex', 'FontSize', 20);
xticks(0:161:1610);
xticklabels({'0', '1','2','3','4','5','6','7', '8', '9', '10'})
xlabel('cycles') 
grid on;