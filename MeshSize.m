classdef MeshSize < int32
    enumeration
        ExtremelyCoarse(9),
        ExtraCoarse(8),
        Coarser(7),
        Coarse(6),
        Normal(5),
        Fine(4),
        Finer(3),
        ExtraFine(2),
        ExtremelyFine(1),
        
        % do not override mesh settings
        Default(-1)
    end
    
    methods (Access = public)
        function out = get_idx(obj)
            out = int32(obj);
        end
        
        function out = is_default(obj)
            out = (obj == MeshSize.Default);
        end
    end
end