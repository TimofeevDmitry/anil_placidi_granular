function write_log(dest_folder, str)

    fid = fopen([dest_folder filesep 'log.txt'], 'a+');
    fprintf(fid, '%s \n\n\n ', str);
    fclose(fid);

end

