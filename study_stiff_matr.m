clear;
clc;

folder = 'D:\Documents\PhD\PROJECTS\GRANULAR\ANIL_PLACIDI_GRANULAR_Git\results\14-Jan-2022_16.56.16_DESKTOP-8VDHUIC\';

state_folder = [folder filesep 'data' filesep 'states'];

n_iter = 150;

json_obj = jsondecode(fileread([folder filesep 'parameters.json']));
Kp = json_obj.kp;

force_disp = importdata([folder, filesep, 'data', filesep, 'force_disp.txt']);
displ = force_disp(1:n_iter + 1, 1);

n_dim_C = 3;
n_dim_D = 6;

det_C = zeros(1, n_iter + 1);
eig_values_C = zeros(n_dim_C, n_iter + 1);

det_D = zeros(1, n_iter + 1);
eig_values_D = zeros(n_dim_D, n_iter + 1);
D_Kp = zeros(n_iter + 1, n_dim_D, n_dim_D);

point_idx = 1;
for it = 0:1:n_iter
    file_name = [state_folder filesep sprintf('state_%d.mat', it)];
    
    load(file_name, 'state');
    
    C = zeros(n_dim_C, n_dim_C);
    C_src = zeros(2, 2, 2, 2);
    
    idx_C = 1;
    for i = 1:2
        for j = 1:2
            for k = 1:2
                for l = 1:2
                    C_src(i, j, k, l) = state.C(point_idx, idx_C);
                    idx_C = idx_C + 1;
                end
            end
        end
    end
    C(1, 1) = C_src(1, 1, 1, 1);
    C(1, 2) = C_src(1, 1, 2, 2);
    C(1, 3) = sqrt(2) * C_src(1, 1, 1, 2);
    C(2, 1) = C_src(2, 2, 1, 1);
    C(2, 2) = C_src(2, 2, 2, 2);
    C(2, 3) = sqrt(2) * C_src(2, 2, 1, 2);
    C(3, 1) = sqrt(2) * C_src(1, 2, 1, 1);
    C(3, 2) = sqrt(2) * C_src(1, 2, 2, 2);
    C(3, 3) = 2 * C_src(1, 2, 1, 2);

    det_C(it + 1) = det(C);
    eig_values_C(:, it + 1) = eig(C);

    D = zeros(n_dim_D, n_dim_D);
    D_src = zeros(2, 2, 2, 2, 2, 2);
    
    idx_D = 1;
    for i = 1:2
        for j = 1:2
            for k = 1:2
                for l = 1:2
                    for m = 1:2
                        for n = 1:2
                            D_src(i, j, k, l, m, n) = state.D(point_idx, idx_D);
                            idx_D = idx_D + 1;
                        end
                    end
                end
            end
        end
    end
    D(1, 1) = D_src(1, 1, 1, 1, 1, 1);
    D(1, 2) = D_src(1, 1, 1, 2, 2, 1);
    D(1, 3) = D_src(1, 1, 1, 1, 2, 2);
    D(1, 4) = D_src(1, 1, 1, 2, 2, 2);
    D(1, 5) = D_src(1, 1, 1, 1, 1, 2);
    D(1, 6) = D_src(1, 1, 1, 1, 2, 1);
    D(2, 1) = D_src(2, 2, 1, 1, 1, 1);
    D(2, 2) = D_src(2, 2, 1, 2, 2, 1);
    D(2, 3) = D_src(2, 2, 1, 1, 2, 2);
    D(2, 4) = D_src(2, 2, 1, 2, 2, 2);
    D(2, 5) = D_src(2, 2, 1, 1, 1, 2);
    D(2, 6) = D_src(2, 2, 1, 1, 2, 1);
    D(3, 1) = D_src(1, 2, 2, 1, 1, 1);
    D(3, 2) = D_src(1, 2, 2, 2, 2, 1);
    D(3, 3) = D_src(1, 2, 2, 1, 2, 2);
    D(3, 4) = D_src(1, 2, 2, 2, 2, 2);
    D(3, 5) = D_src(1, 2, 2, 1, 1, 2);
    D(3, 6) = D_src(1, 2, 2, 1, 2, 1);
    D(4, 1) = D_src(2, 2, 2, 1, 1, 1);
    D(4, 2) = D_src(2, 2, 2, 2, 2, 1);
    D(4, 3) = D_src(2, 2, 2, 1, 2, 2);
    D(4, 4) = D_src(2, 2, 2, 2, 2, 2);
    D(4, 5) = D_src(2, 2, 2, 1, 1, 2);
    D(4, 6) = D_src(2, 2, 2, 1, 2, 1);    
    D(5, 1) = D_src(1, 1, 2, 1, 1, 1);
    D(5, 2) = D_src(1, 1, 2, 2, 2, 1);
    D(5, 3) = D_src(1, 1, 2, 1, 2, 2);
    D(5, 4) = D_src(1, 1, 2, 2, 2, 2);
    D(5, 5) = D_src(1, 1, 2, 1, 1, 2);
    D(5, 6) = D_src(1, 1, 2, 1, 2, 1);
    D(6, 1) = D_src(1, 2, 1, 1, 1, 1);
    D(6, 2) = D_src(1, 2, 1, 2, 2, 1);
    D(6, 3) = D_src(1, 2, 1, 1, 2, 2);
    D(6, 4) = D_src(1, 2, 1, 2, 2, 2);
    D(6, 5) = D_src(1, 2, 1, 1, 1, 2);
    D(6, 6) = D_src(1, 2, 1, 1, 2, 1);
    
    det_D(it + 1) = det(D);
    eig_values_D(:, it + 1) = eig(D);
     
end

% eig_values_C = sort(eig_values_C);
% eig_values_D = sort(eig_values_D);

for i = 0:1:n_iter
    cond_number_C(i + 1) = max(eig_values_C(:, i + 1)) ./ min(eig_values_C(:, i + 1));
    cond_number_D(i + 1) = max(eig_values_D(:, i + 1)) ./ min(eig_values_D(:, i + 1));
end

plot(displ, cond_number_D);
grid on;
hold on;
plot(displ, cond_number_C);


par.dim = n_dim_D;
par.tens = D;
par.eig = eig_values_D;
par.det = det_D;
par.title = 'D';
par.iters = displ;

% %%
% CM = jet(par.dim);
% 
% % plot eigenvalues
% eig_legend(par.dim) = "";
% for i = 1:par.dim
%     plot(par.iters, real(par.eig(i, :)), 'color', CM(i, :), 'LineWidth', 2);
%     hold on;
%     grid on;
%     
%     eig_legend(i) = sprintf('%d', i);
% end
% title(['Eigenvalues for ' par.title]);
% legend(eig_legend);
% 
% set(gca, 'FontSize', 14)

%%
% % plot determinant
% plot(par.iters, par.det, 'LineWidth', 2);
% grid on;
% 
% title(['Determinant for ' par.title]);
% 
% set(gca, 'FontSize', 14)

% %%
% % plot D/Kp relation
% plot(par.iters, D_Kp(:, 1, 1), 'LineWidth', 2);
% grid on;
% title('$D_{1111}/K_p$', 'Interpreter', 'latex');
% set(gca, 'FontSize', 14)





