function model_out = update_model(obj, model, state, points)
    %% dimention
    s = size(points);
    n = s(1);
    
    %% export tensors
    % first gradient
    C = [points state.C(1:n, :)];
    obj.set_table_data(model, 'table_C', C, n);
    
    % chirality
    M = [points state.M(1:n, :)];
    obj.set_table_data(model, 'table_M', M, n);
    
    % second gradient
    D = [points state.D(1:n, :)];
    obj.set_table_data(model, 'table_D', D, n);
    
    % pre-stress
    P = [points state.P(1:n, :)];
    obj.set_table_data(model, 'table_P', P, n);
    
    % pre-hyperstress
    Q = [points state.Q(1:n, :)];
    obj.set_table_data(model, 'table_Q', Q, n);
    
    % additive constant (different for each point)
    R =  [points state.R(1:n)];
    obj.set_table_data(model, 'table_R', R, n);
    
    %% export additional data for making plots
    % damage information
    DAMAGE = [points state.DAMAGE(1:n, :)];
    obj.set_table_data(model, 'table_DAMAGE', DAMAGE, n);
    
    % dissipation energy density
    W = [points state.W(1:n)];
    obj.set_table_data(model, 'table_W', W, n);
    
    % objective relative displacement
    OBJ_REL_DISP = [points state.OBJ_REL_DISP(1:n, :)];
    obj.set_table_data(model, 'table_OBJ_REL_DISP', OBJ_REL_DISP, n);
    
    % plastic information
    if ~obj.no_plasticity
        PLASTICITY = [points state.PLASTICITY(1:n, :)];
        obj.set_table_data(model, 'table_PLASTICITY', PLASTICITY, n);
    end
    
    model_out = model;
end