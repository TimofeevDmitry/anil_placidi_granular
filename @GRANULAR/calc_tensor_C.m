function C_val = calc_tensor_C(obj, i, j, a, b, K)

ni = obj.norm(:, i);
nj = obj.norm(:, j);
na = obj.norm(:, a);
nb = obj.norm(:, b);

n1 = ni .* nj .* na .* nb;

n2 = eq(i, a) * nj .* nb + eq(i, b) * nj .* na + ...
    eq(j, a) * ni .* nb + eq(j, b) * ni .* na;

C_val = obj.L^2 * sum((K.norm .* n1 + K.tang .* (n2 - 4 * n1)) .* obj.JdS);

end

