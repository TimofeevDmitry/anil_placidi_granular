function Q_val = calc_tensor_Q(obj, i, j, h, K, disp_plastic)

ni = obj.norm(:, i);
nj = obj.norm(:, j);
nh = obj.norm(:, h);

Q_val = sum(K.norm .* disp_plastic .* ni .* nj .* nh .* obj.JdS);
Q_val = -0.25 * obj.L^2 * Q_val;

end

