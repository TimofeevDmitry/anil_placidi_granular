function run(sim, folders)

import com.comsol.model.*
import com.comsol.model.util.*

update_console('-initialization');
[model_filename, n_mesh] = sim.init_model(folders.models);

% total number of iterations
n_iterat = length(sim.step_u);

% preallocate memory for the state info,
% or load one of previous states
[state, points, i_start] = sim.init_state(n_mesh);

ModelUtil.showProgress(true);
for i = i_start:n_iterat
    if i > 0
        % increase imposed displacement
        state.u_bar = state.u_bar + sim.step_u(i);
        state.f_bar = state.f_bar + sim.step_f(i);
    end
    
    % intermediate energy density
    E = 0;
    
    % intermediate steps counter
    j = 0;
    
    % intermediate state
    inter_state = state;
    while true
        % construct information message 
        if sim.fixed_point
            iter_msg = '-iteration = %d (of %d)\n--intermediate step %d\n---';
            iter_msg = sprintf(iter_msg, i, n_iterat, j);   
        else
            iter_msg = '-iteration = %d (of %d)\n--';
            iter_msg = sprintf(iter_msg, i, n_iterat);
        end
        
        update_console(iter_msg);

        % read mph file
        update_console([iter_msg 'model loading']);
        model = mphload(model_filename);
        
        update_console([iter_msg 'model updating']);
        model = sim.update_model(model, inter_state, points);
        
        % set imposed displacement and force to the model
        model.param.set('u_bar', state.u_bar);
        model.param.set('f_bar', state.f_bar);

        % solve finite element problem
        update_console([iter_msg 'solving finite element problem by comsol']);
        model.sol('sol1').runAll();
        
        % evaluate current strain and strain gradient
        [G, dG, points] = sim.eval_strain(model, n_mesh);
        
        % update damage, plasticity and energy coefficients
        inter_state = sim.update_state(state, G, dG, n_mesh, ...
            [iter_msg 'calculating coefficients for point = %d (of %d)']);
        
        % save domain discretization
        if i == 0
            save([folders.data, filesep, 'mesh.mat'], 'points');
        end
        
        % if fixed point method is not acivated,
        % we do not perform additional iterations for a given time step
        % and exit the 'while' loop
        if ~sim.fixed_point
            break;
        end
        
        % evaluate current energy density
        Ej = sim.eval_energy(model) + inter_state.W;
        
        % evaluate residual
        if j > 0
            r = max(abs((Ej - E)./E));
        else
            r = 1;
        end
        
        if (j > 0 && r < sim.r_threshold) || i == 0      
            break; % solution is found
        elseif j == sim.r_max_iter
            error('maximum number of iterations reached!');
        end
        
        E = Ej;
        j = j + 1;
    end
    
    % intermediate results become current state
    state = inter_state;
    
    % save calculation data
    update_console([iter_msg 'saving data']);
    sim.save_data(i, model, state, points, folders);
    
    % saving figures
    update_console([iter_msg 'saving figures']);
    sim.save_figures(model, i, folders);
    
    % remove the model
    ModelUtil.clear();
end

update_console('');

end
