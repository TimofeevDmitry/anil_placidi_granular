function P_val = calc_tensor_P(obj, i, j, K, disp_plastic)

ni = obj.norm(:, i);
nj = obj.norm(:, j);

P_val =  -obj.L * sum(K.norm .* disp_plastic .* ni .* nj .* obj.JdS);

end

