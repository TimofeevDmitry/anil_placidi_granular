function R_val = calc_tensor_R(obj, K, disp_plastic)

R_val = 0.5 * sum(K.norm .* (disp_plastic .* disp_plastic) .* obj.JdS);

end

