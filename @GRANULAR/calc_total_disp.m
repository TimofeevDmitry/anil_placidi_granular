function [disp_n, disp_t] = calc_total_disp(obj, G, dG)

s = size(obj.norm);

n_dir = s(1);
n_dim = s(2);

u = zeros(n_dir, n_dim);

for dir = 1:n_dir
    for i = 1:n_dim
        u_g1 = 0;
        u_g2 = 0;
        for j = 1:n_dim
            u_g1 = u_g1 + G(i, j) * obj.norm(dir, j);
            for h = 1:n_dim
                u_g2 = u_g2 + dG(i, j, h) * ...
                    obj.norm(dir, j) * obj.norm(dir, h);
            end
        end
        u(dir, i) = (2 * u_g1 + 0.5 * obj.L * u_g2) * obj.L;
    end
end

dn = dot(u', obj.norm');

disp_n = 0.5 * dn';
disp_t = vecnorm(u' - dn .* obj.norm')';

end
