function save_figures(obj, model, iter, folders)
if obj.no_image_export
    return;
end

labels = [
    "G11", "G12", "G22", "G111", "G112", "G121", "G122", "G221", "G222"
    ];
labels = [labels, "energy", "energy1", "energy2", "energy3"];
labels = [labels, "DMG_NORM_MAX", "DMG_NORM_AVR", "DMG_TANG_MAX", ...
    "DMG_TANG_AVR", "W"];
labels = [labels "u1" "u2" "u_eta" "u_tau"];

if ~obj.no_plasticity
    labels = [
        labels "energy4", "energy5", "energy6"
        ];
    labels = [
        labels "LMT_MAX", "LMT_AVR", "LMC_MAX", "LMC_AVR", "PLASTIC_AVR"
        ];
end

for lbl = labels
    image_tag = "image_" + lbl;
    filename = [ folders.figures, filesep, char(lbl), filesep, ...
        sprintf('%s_%d.png', lbl, iter)];

    model.result.export(image_tag).set('pngfilename', filename);
    model.result.export(image_tag).run;
end

end