function D_val = calc_tensor_D(obj, i, j, h, a, b, c, K)

ni = obj.norm(:, i);
nj = obj.norm(:, j);
nh = obj.norm(:, h);
na = obj.norm(:, a);
nb = obj.norm(:, b);
nc = obj.norm(:, c);

n1 = ni .* nj .* nh .* na .* nb .* nc;

n2 = (eq(i, a) * nj .* nb + eq(i, b) * nj .* na + ...
    eq(j, a) * ni .* nb + eq(j, b) * ni .* na) .* nh .* nc;

D_val1 = sum((K.norm .* n1 + K.tang .* (n2 - 4 * n1)) .* obj.JdS);
D_val2 = sum(n1 .* obj.JdS);

D_val = (0.0625 * obj.L^4 * D_val1 + obj.L^2 * obj.kp * D_val2);

end

