function save_data(sim, i, model, state, points, folders)

%% save comsol model to *.mph file
if sim.save_mph && ...
        (isempty(sim.save_mph_indices) || ismember(i, sim.save_mph_indices))
    model.save([folders.models filesep sprintf('model_%d.mph', i)]);
end

%% write damage and plasticity
Dn  = state.Dn;
Dt  = state.Dt;

if sim.save_damage && ...
        (isempty(sim.save_damage_indices) || ismember(i, sim.save_damage_indices))
    save([folders.damage, filesep, sprintf('damage_%d.mat', i)], 'Dn', 'Dt');
end

if ~sim.no_plasticity
    Lt = state.Lt;
    Lc = state.Lc;
    
    if sim.save_plasticity && ...
            (isempty(sim.save_plasticity_indices) || ismember(i, sim.save_plasticity_indices))
        save([folders.plasticity, filesep, sprintf('plasticity_%d.mat', i)], ...
            'Lt', 'Lc');
    end
    
    
end
    
%% write force/displacement
td = model.result.table('table_lm').getReal();

fd_line = sprintf('%d %d ', state.u_bar, state.f_bar);
for j = 1:length(td)
    fd_line = sprintf([fd_line '%d '], td(j));
end

fd_file = fopen([folders.data filesep 'force_disp.txt'], 'at');
fprintf(fd_file, "%s\n", fd_line);
fclose(fd_file);

%% save state
if sim.save_state && ...
        (isempty(sim.save_state_indices) || ismember(i, sim.save_state_indices))
    save([folders.states, filesep, sprintf('state_%d.mat', i)], ...
        'state', 'points', 'i');
end

end

