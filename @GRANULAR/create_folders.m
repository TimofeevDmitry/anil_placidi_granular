function [folders] = create_folders(obj)

out_folder_name = obj.out_folder_name;
if isempty(out_folder_name)
    date_and_time_str = datestr(datetime('now'), 'dd-mmm-yyyy_HH.MM.SS');
    [~, pc_name] = system('hostname');
    
    pc_name = pc_name(1:end-1);
    
    out_folder_name = [date_and_time_str '_' pc_name];
end

res_f =  [pwd filesep 'results' filesep out_folder_name];

if exist(res_f, 'dir')
    % overwriting is not allowed
    error('folder <%s> already exists!', res_f);
end

folders.result = res_f;

folders.data = [folders.result, filesep, 'data'];
folders.models = [folders.result, filesep, 'models'];
folders.figures = [folders.result, filesep, 'figures'];

folders.damage = [folders.data, filesep, 'damage'];
folders.plasticity = [folders.data, filesep, 'plasticity'];

folders.states = [folders.data, filesep, 'states'];

mkdir(folders.result);

mkdir(folders.data);
mkdir(folders.models);
mkdir(folders.figures);

mkdir(folders.damage);
mkdir(folders.plasticity);

mkdir(folders.states)

end