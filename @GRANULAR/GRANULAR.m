classdef GRANULAR
    properties (Access = public)
        n_dim = 2;
      
        % discretization over orientation
        n_theta {mustBeNumeric}; % for 2D and 3D
        n_phi   {mustBeNumeric}; % only for 3D
        
        % J(phi) * dS = sin(phi) * d_theta * d_phi 
        JdS {mustBeNumeric};

        % stiffness coefficients
        knt {mustBeNumeric};    % normal tension
        knc {mustBeNumeric};	% normal compression
        kw  {mustBeNumeric};	% tangential
        kp  {mustBeNumeric};    % pantographic
        
        % coefficient responsible for normal stiffness regularization
        % (set <= 0 to remove regularization)
        kn_smooth {mustBeNumeric}
    
        % parameters of damage evolution
        Bc {mustBeNumeric};     % compression
        Bt {mustBeNumeric};     % tension
        Bw0 {mustBeNumeric};    % tangential
        a1 {mustBeNumeric};     % related to calculation of tangential
        a2 {mustBeNumeric};     % related to calculation of tangential
        
        % parameters for plasticity
        snt {mustBeNumeric};
        snc {mustBeNumeric}
    
        % size of microstructure
        L {mustBeNumeric};
        
        % max possible value for damage
        damage_threshold = 1;
        
        % intermediate convergence
        r_threshold = 1
        
        % maximum numer of intermediate iterations
        r_max_iter = 25;
        
        % normal direction
        norm {mustBeNumeric};
        
        % tangential direction
        tang {mustBeNumeric};
        
        % name of the output folder (in .\results folder)
        out_folder_name = '';
        
        % name of comsol file (in .\comsol folder)
        model_filename = '';
        
        % name of the file, where the state of initialization is stored
        state_filename = '';
        
        %
        no_image_export = false;
        
        % size of mesh
        mesh_size;
        
        % curvature factor for meshing
        curve_factor;
        
        % steps description
        step_u {mustBeNumeric};
        step_f {mustBeNumeric}
        
        %
        save_damage = true;
        save_damage_indices = [];
        
        %
        save_plasticity = true;
        save_plasticity_indices = [];
        
        %
        save_mph = false;
        save_mph_indices = [];
        
        %
        save_state = false;
        save_state_indices = [];
        
        % disable calculation of plastic information
        no_plasticity = false;
        
        fixed_point = true;
    end
    
    % public methods declaration
    methods (Access = public)
        
        function sim = GRANULAR(file_name)
            % name of properties which can not be automatically copied
            non_copy_props = [ 
                "mesh_size", "steps", "save_mph_indices"
                ];
            
            % first argument has to be filename
            json_obj = jsondecode(fileread(file_name));
            
            props = fieldnames(json_obj);
            
            % copy properties
            for i = 1:1:length(props)
                if ismember(props{i}, non_copy_props)
                    continue;
                end
                sim.(props{i}) = json_obj.(props{i});
            end
            
            % init mesh_size property
            if ~isfloat(json_obj.mesh_size)
                sim.mesh_size = MeshSize(json_obj.mesh_size);
            else
                sim.mesh_size = json_obj.mesh_size;
            end
            %
            
            % init step_u property
            sim.step_u = zeros(1, json_obj.steps(end).fin);
            sim.step_f = zeros(1, json_obj.steps(end).fin);
            start  = 1;
            finish = 1;
            for i = 1:length(json_obj.steps)
                s = json_obj.steps(i);
                if finish > s.fin || ~isfloat(s.u_val) || ~isfloat(s.f_val) 
                    error('Error: steps description is not correct')
                end
                
                finish = s.fin;
                sim.step_u(start:finish) = s.u_val;
                sim.step_f(start:finish) = s.f_val;
                
                start = finish + 1;
            end
            
            %
            for prop_lbl = [ ...
                    "save_mph_indices", "save_state_indices", ...
                    "save_damage_indices", "save_plasticity_indices", ...
                    ]
                indices = json_obj.(prop_lbl);
                if ischar(indices)
                    try
                        indices = eval(json_obj.(prop_lbl));
                        indices = unique([1, indices, json_obj.steps(end).fin]);
                    catch
                        indices = [];
                    end
                end
                sim.(prop_lbl) = indices;
            end
            
            % discretization over orientation
            d_theta = (2 * pi) / sim.n_theta;
            
            if sim.n_dim == 2
                % over circle
                theta = (0:d_theta: 2 * pi - d_theta);
                
                X = cos(theta);
                Y = sin(theta);
                
                sim.norm = [X; Y]';
                
                sim.JdS = ones(sim.n_theta, 1) * d_theta;
                
            elseif sim.n_dim == 3       
                % over sphere
                d_phi = (pi / sim.n_phi) / sim.n_theta;
                
                theta = 0:d_theta:((2 * sim.n_phi * pi));
                phi   = 0:d_phi:pi;
                
                X = cos(theta) .* sin(phi);
                Y = sin(theta) .* sin(phi);
                Z = cos(phi);
                
                sim.norm = [X; Y; Z]';

                sim.JdS = sin(phi)' * d_theta * d_phi;
            end
        end
        
        % main method to start the simulation
        run(obj, folders);
        
        % save calculation data
        save_data(sim, i, model, state, points, folders);
        
        % create output folders
        % where all data and figures are stored
        folders = create_folders(sim);
        
        % calculation of damage, plasticity and all the coefficients
        % for j-th point of the model
        state = update_state(sim, state, G, dG, n_mesh, msg);

        % calculation of objective relative displacement
        % (total, plastic and elastic)
        % for j-th point
        [disp_n, disp_t] = calc_total_disp(sim, G, dG);
        
        % calculation of Btau function and its derivative
        % with respect to displacement
        function [Bw_val, dBw_val] = calc_Bw(sim, de)
            alpharatio = ((1 - sim.a1) / sim.a2) * sim.Bw0;
            
            dn1 = de >= 0;
            dn2 = (de >= alpharatio) & (de < 0);
            dn3 = (de  < alpharatio) & (de < 0);
            
            Bw_val = dn1 * sim.Bw0;
            Bw_val = Bw_val + dn2 .* (sim.Bw0 - sim.a2 * de);
            Bw_val = Bw_val + dn3 .* (sim.a1 * sim.Bw0);
            
            dBw_val = dn2 * (-sim.a2);
        end 
        
        % calculation of damage for normal and tangential directions
        % for i-th direction
        function [Dn, Dt] = calc_damage(sim, disp_ne, disp_t, Bw)

            disp_el1 = disp_ne .* (disp_ne >= 0);
            disp_el2 = disp_ne .* (disp_ne  < 0);
            
            Dn1 = 1 - exp(-disp_el1 / sim.Bt);
            Dn2 = (2/pi) .* atan(abs(disp_el2 / sim.Bc));
            
            Dn = Dn1 + Dn2;
            
            Dt = 1 - exp(-abs(disp_t) ./ Bw);
            
            cond_Dn = Dn >= sim.damage_threshold;
            Dn = cond_Dn .* sim.damage_threshold + (~cond_Dn) .* Dn;
            
            cond_Dt = Dt > sim.damage_threshold;
            Dt = cond_Dt .* sim.damage_threshold + (~cond_Dt) .* Dt;
        end
        
        function [Kn, Kt] = calc_K(sim, de, Dn, Dt)
            Kn = sim.calc_Kn_undamaged(de);
            Kn = Kn .* (1 - Dn);
            
            Kt = sim.kw .* (1 - Dt);
        end
        
        function Kn = calc_Kn_undamaged(sim, dn) 
            if sim.kn_smooth > 0
                % regularization on
                h1 = heaviside_smooth(dn, sim.kn_smooth);
                h2 = heaviside_smooth(-dn, sim.kn_smooth);
                
                Kn = (sim.knt * h1 + sim.knc * h2);
            else
                % regularization off
                cond = dn < 0;
                Kn = (~cond) * sim.knt + cond * sim.knc;
            end
        end
        
        function [Lmt, Lmc] = calc_plasticity(sim, state, dn, Kn, Dt, Bw, dBw, j)    
            log_t = log(1 - Dt);
            
            I = sim.kw .* Bw .* dBw .* ...
                (2 + (Dt - 1) .* (2 - 2 * log_t + log_t .* log_t));
            
            Lmt =  dn + state.Lc(j, :)' - (sim.snt - I) ./ Kn;
            Lmc = -dn + state.Lt(j, :)' - (sim.snc + I) ./ Kn;
        end
        
        function Wn = calc_W_dam_norm(sim, de, Dn)
            cond = (de < 0);            
            
            D1 = cond .* Dn;
            D2 = (~cond) .* Dn;
            
            log_n = log(1 - D2);
        
            Wn1 = sim.knc .* (sim.Bc^2) .* ...
                (-D1 + (2/pi) .* tan((pi/2) .* D1));
            Wn2 = sim.knt .* (sim.Bt^2) .* ...
                (2 + (D2 - 1) .* (2 - 2 * log_n + log_n .* log_n));
            
            Wn = 0.5 * (Wn1 + Wn2);
        end
        
        function Wt = calc_W_dam_tang(sim, Dt, Bw)
            log_t = log(1 - Dt);

            Wt = 0.5 * sim.kw .* ...
                (Bw .^ 2) .* (2 + (Dt - 1) .* (2 - 2 * log_t + log_t .* log_t));
        end
        
        function Wp = calc_W_plastic(sim, Lt, Lc)
            Wp = sim.snt * Lt + sim.snc * Lc;
        end
        
        % elastic coefficients
        C_val = calc_tensor_C(sim, i, j, a, b, K);
        M_val = calc_tensor_M(sim, i, j, a, b, c, K);
        D_val = calc_tensor_D(sim, i, j, h, a, b, c, K);
        P_val = calc_tensor_P(sim, i, j, K, disp_plastic);
        Q_val = calc_tensor_Q(sim, i, j, h, K, disp_plastic);
        R_val = calc_tenspr_R(sim, K, disp_plastic);
        
        % first model loading and meshing 
        % save new comsol file with meshed model in a given folder
        % returns a name of the file and number of points in the mesh
        [filename, mesh] = init_model(sim, folder);
        
        % init state structure
        [state, points, i_start] = init_state(sim, n_mesh);
        
        % update comsol model
        model_out = update_model(sim, model, state, x, y, n);
         
        % evaluate strain, strain gradient
        % and coordinates of mesh points
        [G, dG, coords] = eval_strain(sim, model, n_mesh);
    end
    
    methods (Static, Access = public)
        
        % evaluate current energy density
        E = eval_energy(model);
        
        % evaluate current displacement
        U = eval_disp(model);
        
        function set_table_data(model, table_tag, table_data, n)
            model.result.table(table_tag).set('tablebuffersize',  n);
            model.result.table(table_tag).setTableData(table_data);
        end
        
        % save domain discretization
        function save_discr(coords, folders)
            file = fopen([folders.data, filesep, 'discr.txt'], 'at');
            fprintf(file, "%30.16f%30.16f%30.16f\n", coords');
            fclose(file);
        end
    end
end

