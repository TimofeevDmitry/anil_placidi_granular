function state = update_state(sim, state, G, dG, n_mesh, msg)
    n_dim = sim.n_dim;

    for p = 1:1:n_mesh
        if strlength(msg) > 0
            update_console(sprintf(msg, p, n_mesh));
        end
        
        % strain and strain gradient for a given point
        cur_G  = squeeze(G(p, :, :));
        cur_dG = squeeze(dG(p, :, :, :));
        
        % total and elastic displacement
        [disp_nt, disp_tt] = sim.calc_total_disp(cur_G, cur_dG);
        disp_ne = disp_nt - state.Lt(p, :)' + state.Lc(p, :)';
    
        % calc Bw
        [Bw, dBw] = sim.calc_Bw(disp_ne);
        
        % calc current damage
        [Dn, Dt] = sim.calc_damage(disp_ne, disp_tt, Bw);
        
        % update damage state
        idxs_Dn = find(Dn - state.Dn(p, :)' > 0);
        idxs_Dt = find(Dt - state.Dt(p, :)' > 0);
        
        state.Dn(p, idxs_Dn) = Dn(idxs_Dn);
        state.Dt(p, idxs_Dt) = Dt(idxs_Dt);
        
        Dn = state.Dn(p, :)';
        Dt = state.Dt(p, :)';
        
        % calc current stiffnesses
        [K.norm, K.tang] = sim.calc_K(disp_ne, Dn, Dt);
        
        % update dissipation energy info
        state.Wn(p, idxs_Dn) = ...
            sim.calc_W_dam_norm(disp_ne(idxs_Dn), Dn(idxs_Dn));
        state.Wt(p, idxs_Dt) = ...
            sim.calc_W_dam_tang(Dt(idxs_Dt), Bw(idxs_Dt));
        
        if ~sim.no_plasticity
            % calc plasticity variables
            [Lt, Lc] = ...
                sim.calc_plasticity(state, disp_nt, K.norm, Dt, Bw, dBw, p);
            
            % update plastic state
            idxs_Lt = find(Lt - state.Lt(p, :)' > 0);
            idxs_Lc = find(Lc - state.Lc(p, :)' > 0);
            
            state.Lt(p, idxs_Lt) = Lt(idxs_Lt);
            state.Lc(p, idxs_Lc) = Lc(idxs_Lc);
            
            Lt = state.Lt(p, :)';
            Lc = state.Lc(p, :)';
            
            state.Wp(p, :) = sim.calc_W_plastic(Lt, Lc);
        end
        
        % new value for dissipation energy
        state.W(p) = ...
            sum((state.Wn(p, :) + state.Wt(p, :) + state.Wp(p, :)) .* sim.JdS');
        
        % max and average damage
        state.DAMAGE(p, 1) = max(Dn);
        state.DAMAGE(p, 2) = mean(Dn);
        state.DAMAGE(p, 3) = max(Dt);
        state.DAMAGE(p, 4) = mean(Dt);
        
        % average objective relative displacements
        state.OBJ_REL_DISP(p, 1) = mean(disp_ne);
        state.OBJ_REL_DISP(p, 2) = mean(disp_tt);   
        
        idxs.C = 0;
        idxs.M = 0;
        idxs.D = 0;
        idxs.P = 0;
        idxs.Q = 0;

        for i = 1:n_dim
            for j = 1:n_dim
                for h = 1:n_dim
                    for a = 1:n_dim
                        idxs.C = idxs.C + 1;
                        state.C(p, idxs.C) = ...
                            sim.calc_tensor_C(i, j, h, a, K);
                        for b = 1:n_dim
                            idxs.M = idxs.M + 1;
                            state.M(p, idxs.M) = ...
                                sim.calc_tensor_M(i, j, h, a, b, K);
                            for c = 1:n_dim
                                idxs.D = idxs.D + 1;
                                state.D(p, idxs.D) = ...
                                    sim.calc_tensor_D(i, j, h, a, b, c, K);
                            end
                        end
                    end
                end
            end
        end
        
        if ~sim.no_plasticity
            disp_np = Lt - Lc;
            
            state.R(p) = sim.calc_tensor_R(K, disp_np);
            for i = 1:n_dim
                for j = 1:n_dim
                    idxs.P = idxs.P + 1;
                    state.P(p, idxs.P) = ...
                        sim.calc_tensor_P(i, j, K, disp_np);
                    for h = 1:n_dim
                        idxs.Q = idxs.Q + 1;
                        state.Q(p, idxs.Q) = ...
                            sim.calc_tensor_Q(i, j, h, K,  disp_np);
                    end
                end
            end
            
            state.PLASTICITY(p, 1) = max(Lt);
            state.PLASTICITY(p, 2) = mean(Lt);
            state.PLASTICITY(p, 3) = max(Lc);
            state.PLASTICITY(p, 4) = mean(Lc);
            state.PLASTICITY(p, 5) = mean(disp_np);
        end
    end
end
