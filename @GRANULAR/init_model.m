function [meshed_model_filename, n_mesh] = init_model(obj, folder)

% initial loading
model = mphload(['comsol' filesep obj.model_filename]);

% set up mesh size
if ~isfloat(obj.mesh_size)
    if ~obj.mesh_size.is_default()
        % automatic mesh size by comsol
        model.mesh('mesh1').feature('size').set('custom', 'off');
        model.mesh('mesh1').feature('size').set('hauto', obj.mesh_size.get_idx());
        model.mesh('mesh1').run; % rebuild mesh
    end
else
    % maximum element size is defined by user
    model.mesh('mesh1').feature('size').set('custom', 'on');
    model.mesh('mesh1').feature('size').set('hmax', obj.mesh_size);
    model.mesh('mesh1').feature('size').set('hcurve', obj.curve_factor);
    model.mesh('mesh1').run; % rebuild mesh
end

% get mesh size
[~, data] = mphmeshstats(model);
n_mesh = length(data.vertex);

% save model after meshing
meshed_model_filename = [folder filesep 'model_ref.mph'];
model.save(meshed_model_filename);

end