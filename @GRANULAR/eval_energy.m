function E = eval_energy(model)

% current energy density
E_eval = mpheval(model, 'energy');
E = E_eval.d1';

end

