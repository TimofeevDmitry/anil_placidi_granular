function M_val = calc_tensor_M(obj, i, j, a, b, c, K)

ni = obj.norm(:, i);
nj = obj.norm(:, j);
na = obj.norm(:, a);
nb = obj.norm(:, b);
nc = obj.norm(:, c);

n1 = ni .* nj .* na .* nb .* nc;

n2 = (eq(i, a) * nj .* nb + eq(i, b) * nj .* na +...
    eq(j, a) * ni .* nb + eq(j, b) * ni .* na) .* nc;

M_val = sum((K.norm .* n1 + K.tang .* (n2  - 4 * n1)) .* obj.JdS);
M_val = 0.25 * obj.L^3 * M_val;

end

