function [G, dG, points] = eval_strain(sim, model, n_mesh)

n_dim = sim.n_dim;

% allocate memory for strain and strain gradient
G  = zeros(n_mesh, n_dim, n_dim);
dG = zeros(n_mesh, n_dim, n_dim, n_dim);

% evaluate strain and strain gradient
for i = 1:sim.n_dim
    for j = 1:sim.n_dim
        eval = mpheval(model, sprintf('G%d%d', i, j));
        G(:, i, j) = eval.d1'; 
        for h = 1:sim.n_dim
            eval = mpheval(model, sprintf('G%d%d%d', i, j, h));
            dG(:, i, j, h) = eval.d1';
        end
    end
end

% extract coordinates of mesh points
points = zeros(n_mesh, n_dim);
for i = 1:n_dim
    points(:, i) = eval.p(i, :);
end

end

