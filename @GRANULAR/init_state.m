function [state, points, i] = init_state(sim, n_mesh)

if isempty(sim.state_filename)
    s = size(sim.norm);
    
    n_dir = s(1);
    n_dim = s(2);
    
    % preallocate memory for coefficients
    state.C  = zeros(n_mesh, n_dim ^ 4);
    state.M  = zeros(n_mesh, n_dim ^ 5);
    state.D  = zeros(n_mesh, n_dim ^ 6);
    state.P  = zeros(n_mesh, n_dim ^ 2);
    state.Q  = zeros(n_mesh, n_dim ^ 3);
    state.R  = zeros(n_mesh, 1);
    
    % preallocate memory for damage, plasticity
    % and dissipation energy
    state.Dn = zeros(n_mesh, n_dir);
    state.Dt = zeros(n_mesh, n_dir);
    state.Lt = zeros(n_mesh, n_dir);
    state.Lc = zeros(n_mesh, n_dir);
    
    state.Wn = zeros(n_mesh, n_dir);
    state.Wt = zeros(n_mesh, n_dir);
    state.Wp = zeros(n_mesh, n_dir);
    
    % preallocate memory for information to be plotted by comsol
    state.W            = zeros(n_mesh, 1);
    state.DAMAGE       = zeros(n_mesh, 4);
    state.PLASTICITY   = zeros(n_mesh, 5);
    state.OBJ_REL_DISP = zeros(n_mesh, 2);
    
    % variables to rule boundary conditions
    state.u_bar = 0;    % displacement
    state.f_bar = 0;    % force
    
    % calculate initial values for coefficient
    zero_G  = zeros(1, n_dim, n_dim);
    zero_dG = zeros(1, n_dim, n_dim, n_dim);
    state = sim.update_state(state, zero_G, zero_dG, 1, '');
    
    points = zeros(1, n_dim);
  
    i = 0;
else
    load(sim.state_filename, 'state', 'points', 'i');
    
    i = i + 1;
end

end

