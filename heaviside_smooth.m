function out = heaviside_smooth(x, coeff)
    % smooth alternative for the Heaviside function
    out = 0.5 .* (1 + (2/pi) .* atan(x ./ coeff));
end
